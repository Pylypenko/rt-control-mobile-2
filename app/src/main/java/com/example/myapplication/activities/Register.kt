package com.example.myapplication.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.R
import com.example.myapplication.api.RetrofitClient
import com.example.myapplication.models.RegisterResponse
import kotlinx.android.synthetic.main.activity_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Register : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        textViewLogin.setOnClickListener {
            startActivity(Intent(this@Register, LoginActivity::class.java))
        }
        button.setOnClickListener {
            val email = editTextEmail.text.toString().trim();
            val password = editTextPassword.text.toString().trim();


            if (email.isEmpty()) {
                editTextEmail.error = "Email required"
                editTextEmail.requestFocus()
                return@setOnClickListener
            }
            if (password.isEmpty()) {
                editTextPassword.error = "Password required"
                editTextPassword.requestFocus()
                return@setOnClickListener
            }
            RetrofitClient.instance.registerUser(email, password, password)
                .enqueue(object : Callback<RegisterResponse> {
                    override fun onFailure(call: Call<RegisterResponse>, t: Throwable) {
                        Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG)
                            .show()
                    }

                    override fun onResponse(
                        call: Call<RegisterResponse>,
                        response: Response<RegisterResponse>
                    ) {
                        Toast.makeText(applicationContext, response.toString(), Toast.LENGTH_LONG)
                            .show()
                        startActivity(Intent(this@Register, TakenQuizes::class.java))

                    }
                })
        }
    }
}
