package com.example.myapplication.activities

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import com.example.myapplication.R
import com.example.myapplication.api.RetrofitClient
import com.example.myapplication.models.LoginRequest
import com.example.myapplication.models.LoginResponse
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        textViewRegister.setOnClickListener {
            startActivity(Intent(this@LoginActivity, Register::class.java))

        }
        button.setOnClickListener {
            val email = editTextEmail.text.toString().trim();
            val password = editTextPassword.text.toString().trim();
            fun showNotification(title: String, message: String) {
                val mNotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    val channel = NotificationChannel(
                        "YOUR_CHANNEL_ID",
                        "YOUR_CHANNEL_NAME",
                        NotificationManager.IMPORTANCE_DEFAULT
                    )
                    channel.description = "YOUR_NOTIFICATION_CHANNEL_DESCRIPTION"
                    mNotificationManager.createNotificationChannel(channel)
                }
                val mBuilder =
                    NotificationCompat.Builder(applicationContext, "YOUR_CHANNEL_ID")
                        .setSmallIcon(R.mipmap.ic_launcher) // notification icon
                        .setContentTitle(title) // title for notification
                        .setContentText(message)// message for notification
                        .setAutoCancel(true) // clear notification after click
                val intent = Intent(applicationContext, LoginActivity::class.java)
                val pi = PendingIntent.getActivity(
                    this@LoginActivity,
                    0,
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT
                )
                mBuilder.setContentIntent(pi)
                mNotificationManager.notify(0, mBuilder.build())
            }


            if (email.isEmpty()) {
                editTextEmail.error = "Email required"
                editTextEmail.requestFocus()
                return@setOnClickListener
            }
            if (password.isEmpty()) {
                editTextPassword.error = "Password required"
                editTextPassword.requestFocus()
                return@setOnClickListener
            }
            val request = LoginRequest("admin", "admin@example.com", "admin")
            RetrofitClient.instance.loginUser(request)
                .enqueue(object : Callback<LoginResponse> {
                    override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                        Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
                    }

                    override fun onResponse(
                        call: Call<LoginResponse>,
                        response: Response<LoginResponse>
                    ) {
                        Toast.makeText(applicationContext, response.toString(), Toast.LENGTH_LONG)
                            .show()
                        startActivity(Intent(this@LoginActivity, TakenQuizes::class.java))

                    }
                })


        }
    }
}

