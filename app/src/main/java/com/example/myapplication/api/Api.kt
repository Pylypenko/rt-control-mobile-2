package com.example.myapplication.api


import com.example.myapplication.models.*
import retrofit2.Call
import retrofit2.http.*

interface Api {

    @FormUrlEncoded
    @POST("rest-auth/registration")
    fun registerUser(
        @Field("email") email: String,
        @Field("password1") password1: String,
        @Field("password2") password2: String

    ): Call<RegisterResponse>

    @POST("rest-auth/login")
    fun loginUser(
      @Body data: LoginRequest


    ): Call<LoginResponse>

    @GET("app/quiz/available_quizes")
    fun getAvailableQuizes(
    ): Call<QuizReadonlySwaggerList>


    @GET("app/quiz/taken_quizes")
    fun getTakenQuizes(

    ): Call<QuizResultListReadonlySwagger>


}

