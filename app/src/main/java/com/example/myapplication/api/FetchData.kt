package com.example.myapplication.api


import android.os.AsyncTask
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import org.json.JSONObject


class GetTakenQuizes : AsyncTask<String, Void, JSONObject>() {
    override fun doInBackground(vararg params: String?): JSONObject {
        val accessToken = Token.token
        val client = OkHttpClient()
        val request = Request.Builder()
            .url("https://serhii.univprofect.com/app/quizes/taken_quizes")
            .addHeader("Authorization", accessToken)
            .build()
        val response = client.newCall(request).execute()
        val jsonResponse = response.body()?.string()
        return JSONObject(jsonResponse)
    }

}

class GetAvailableQuizes : AsyncTask<String, Void, JSONObject>() {
    override fun doInBackground(vararg params: String?): JSONObject {
        val accessToken = Token.token
        val client = OkHttpClient()
        val request = Request.Builder()
            .url("https://serhii.univprofect.com/app/quizes/available_quizes")
            .addHeader("Authorization", accessToken)
            .build()
        val response = client.newCall(request).execute()
        val jsonResponse = response.body()?.string()
        return JSONObject(jsonResponse)
    }

}

class Login : AsyncTask<Any, Void, JSONObject>() {
    override fun doInBackground(vararg params: Any?): JSONObject {
        val client = OkHttpClient()
        val JSON = MediaType.parse("application/json; charset=utf-8")
        val body = RequestBody.create(JSON, params[0].toString())
        val request = Request.Builder()
            .url("https://serhii.univprofect.com/rest-auth/login")
            .method("POST", body)
            .build()
        val response = client.newCall(request).execute()
        val jsonResponse = response.body()?.string()
        return JSONObject(jsonResponse)
    }

}

class Register : AsyncTask<String, Void, JSONObject>() {
    override fun doInBackground(vararg params: String?): JSONObject {
        val client = OkHttpClient()
        val JSON = MediaType.parse("application/json; charset=utf-8")
        val body = RequestBody.create(JSON, params[0].toString())
        val request = Request.Builder()
            .url("https://serhii.univprofect.com/rest-auth/register")
            .method("POST", body)
            .build()
        val response = client.newCall(request).execute()
        val jsonResponse = response.body()?.string()
        return JSONObject(jsonResponse)
    }

}

class PutData : AsyncTask<Any, Void, Void>() {
    override fun doInBackground(vararg params: Any): Void? {
        val accessToken = Token.token
        val client = OkHttpClient()
        val JSON = MediaType.parse("application/json; charset=utf-8")
        val body = RequestBody.create(JSON, params[0].toString())
        val request = Request.Builder()
            .url("https://6mfhd2tjr1.execute-api.eu-west-1.amazonaws.com/prod/movies")
            .addHeader("Authorization", accessToken)
            .method("PUT", body)
            .build()
        client.newCall(request).execute()
        return null
    }

}

class PostData : AsyncTask<Any, Void, Void>() {
    override fun doInBackground(vararg params: Any): Void? {
        val accessToken = Token.token
        val client = OkHttpClient()
        val JSON = MediaType.parse("application/json; charset=utf-8")
        val body = RequestBody.create(JSON, params[0].toString())
        val request = Request.Builder()
            .url("https://6mfhd2tjr1.execute-api.eu-west-1.amazonaws.com/prod/movies")
            .addHeader("Authorization", accessToken)
            .method("POST", body)
            .build()
        client.newCall(request).execute()
        return null
    }

}

class FetchData : AsyncTask<String, Void, JSONObject>() {
    override fun doInBackground(vararg params: String?): JSONObject {
        val accessToken = Token.token
        val client = OkHttpClient()
        val request = Request.Builder()
            .url("https://6mfhd2tjr1.execute-api.eu-west-1.amazonaws.com/prod/movies")
            .addHeader("Authorization", accessToken)
            .build()
        val response = client.newCall(request).execute()
        val jsonResponse = response.body()?.string()
        return JSONObject(jsonResponse)
    }

}
