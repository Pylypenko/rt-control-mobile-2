package com.example.myapplication.models


data class QuizResultListReadonlySwagger(
    val items: QuizReadonlySwagger
)