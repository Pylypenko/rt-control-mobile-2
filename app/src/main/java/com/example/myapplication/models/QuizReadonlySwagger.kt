package com.example.myapplication.models

data class QuizReadonlySwagger(
    val id: String,
    val title: String,
    val start_time: String,
    val end_time: String
)

