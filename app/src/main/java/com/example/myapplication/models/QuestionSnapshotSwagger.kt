package com.example.myapplication.models

data class QuestionSnapshotSwagger(
    val id: String,
    val answers_type: String,
    val text: String,
    val answers: Array<AnswerSnapshotSwagger>
)