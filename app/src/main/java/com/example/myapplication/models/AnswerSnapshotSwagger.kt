package com.example.myapplication.models

data class AnswerSnapshotSwagger(
    val id: String,
    val text: String,
    val is_ticked: Boolean,
    val is_correct: Boolean,
    val points: Boolean
)