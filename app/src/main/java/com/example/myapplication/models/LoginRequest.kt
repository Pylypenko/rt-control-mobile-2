
package com.example.myapplication.models

data class LoginRequest(
    val username: String,
    val email: String,
    val password: String
)