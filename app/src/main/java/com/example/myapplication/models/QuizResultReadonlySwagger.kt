package com.example.myapplication.models

data class QuizResultReadonlySwagger(
    val questions_snapshot: Array<QuestionSnapshotSwagger>,
    val total_result: Number,
    val quiz: QuizReadonlySwagger
)