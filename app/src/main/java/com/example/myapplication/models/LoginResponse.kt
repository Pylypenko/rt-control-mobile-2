package com.example.myapplication.models

data class LoginResponse(
    val username: String,
    val email: String,
    val password: String
)